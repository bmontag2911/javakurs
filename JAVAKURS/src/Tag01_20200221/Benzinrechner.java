package Tag01_20200221;
import java.util.Scanner;

public class Benzinrechner {

	public static void main(String[] args) {
		// Programmieren Sie einen Benzinrechner, 
		// der folgendes leistet: der Anwender kann �ber 2 Eingaben
		// die gefahrenen Kilometer und den Verbrauch in Litern angeben. Ausgegeben werden soll der Verbrauch auf 100 Kilometern.
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie die gefahrenen Kilometer ein.");
		double weg = sc.nextDouble();
		//System.out.println("Gefahrene Kilometer: " + weg);
		System.out.println("Bitte geben Sie den Verbrauch in Litern ein.");
		double verbrauch = sc.nextDouble();
		double verbrauch100 = verbrauch/weg*100; 
		System.out.println("Verbrauch pro 100km: " + verbrauch100 + " Liter");
	}

}
