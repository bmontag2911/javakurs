package Tag01_20200221;
import java.util.Scanner;

public class Sparzinsrechner {

	public static void main(String[] args) {
		// Programmieren Sie einen Sparzinsrechner, 
		// der folgendes leistet: der Anwender kann �ber 
		// 3 Eingaben das Startkapital, die Laufzeit in Jahren sowie den Zinssatz eingeben. 
		// Ausgegeben werden soll das Endkapital am Ende der Laufzeit 
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie das Startkapital in � ein.");
		double startkapital = sc.nextDouble();
		System.out.println("Bitte geben Sie die Laufzeit in Jahren ein.");
		int laufzeit = sc.nextInt();
		System.out.println("Bitte geben Sie den Zinssatz in % ein.");
		double zinssatz = sc.nextDouble();
		//double endkapital = startkapital + (startkapital*zinssatz*laufzeit);
		double endkapital = startkapital * Math.pow((zinssatz/100)+1, laufzeit);
		System.out.printf("Startkapital: %.2f �", startkapital);
		System.out.println("Laufzeit: " + laufzeit + " Jahr(e)");
		System.out.printf("Zinssatz: %.2f %", zinssatz);
		//System.out.println("Endkapital: " + endkapital + " �");
		System.out.printf("Endkapital: %.2f �", endkapital);
	}

}
