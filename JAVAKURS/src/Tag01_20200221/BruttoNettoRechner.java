package Tag01_20200221;
import java.util.Scanner;

public class BruttoNettoRechner {

	public static void main(String[] args) {
		// Der Benutzer soll einen Bruttopreis eingeben k�nnen (Kommazahl)
		// Ausgegeben wird der Nettopreis und die Mehrwertsteuer
		// Testdaten: 119 Euro brutto => 100 Euro netto, 19 Euro Mehrwertsteuer
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie den Bruttopreis ein.");
		double brutto = sc.nextDouble();
		System.out.println("Brutto-Preis: " + brutto + " �");
		double netto = brutto / 1.19; 
		System.out.println("Netto-Preis: " + netto + " �");
		double mwst = brutto - netto; 
		System.out.println("MwSt: " + mwst + " �");
	}

}
