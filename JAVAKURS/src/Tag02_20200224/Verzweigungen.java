package Tag02_20200224;

public class Verzweigungen {

	public static void main(String[] args) {
		int zahl = 6;
//		if(zahl > 0 && zahl%2 == 0) {
//			System.out.println("Die Zahl ist positiv und gerade.");
//		} else {
//			System.out.println("Die Zahl ist nicht positiv und gerade.");
//		}
		
		
		
		zahl = 7;
		if (zahl > 0 && zahl%2 == 0) {
			System.out.println("Die Zahl ist positiv UND gerade.");
		} else if(zahl > 0 || zahl%2 == 0) {
			System.out.println("Die Zahl ist entweder positiv oder gerade.");
		} else {
			System.out.println("Die Zahl ist nicht positiv und ungerade.");
		}
		
	}

}
